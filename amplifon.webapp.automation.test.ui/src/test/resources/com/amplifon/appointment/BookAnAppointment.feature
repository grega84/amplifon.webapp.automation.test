Feature: BookAnAppointment
As a 
I want 
So that 

Scenario: request appointment - e2e
Given the user access to "https://sycleuat.amplifon.com/freecvs/index_classic.php" with followings account
|user|password|
|TestFOA2|Ampl1f0n_test3|
And check the available dataTime slot
|clinicLocation|date|time|
|CANADA - 90001A76 Abbotsford|Monday-June 12|08:15 am|
Given the user access to "https://aem-fe-test.amplifon.com/ca/" portal with followings account
|user |password |
|amplifon_dev|hKZ>y};^2_-5|
And go to "Request appointment" page and compile the form with the followings data
|clinicLocation| appointmentType|date|time|title|fistName|surname|email|phoneNumber|privacyPolicyCheckBox|
|Amplifon Abbosford - Fraser Way|HACK|Monday-June 12|08:15 am|M|pippo|warner|g.rega@prismaprogetti.it|3333333653|true|
When press button "Request an appointment"
Then thank you page is displayed
Then the confermation email is received
|from|contentText|timeoutToreceive|
|ddd|ggggg|20|
And after access to "https://sycleuat.amplifon.com/freecvs/index_classic.php" check the appointment
|clinicLocation|date|time|fistName|surname|
|CANADA - 90001A76 Abbotsford|Monday-June 12|08:15 am|pippo|warner|

Scenario Outline: 
Given the user access to "https://aem-fe-test.amplifon.com/ca/" portal with followings account
|user |password |
|amplifon_dev|hKZ>y};^2_-5|
And go to "Request appointment" page and compile the form with date already selected the followings data 
|clinicLocation| appointmentType|title|fistName|surname|email|phoneNumber|privacyPolicyCheckBox|
|Amplifon Abbosford - Fraser Way|HACK|M|pippo|warner|g.rega@prismaprogetti.it|3333333653|true|
When press button "Request an appointment"
Then the error message are displayed as follow
|field| errorMessage|

Examples: 
| Title | First name | Surname | emailAddress | Phonenumber | alreadyClient | youngerThan26 | earingAids |privacyPolycy|  field| errorMessage|
|       | First name | Surname | emailAddress | Phonenumber | alreadyClient | youngerThan26 | earingAids |privacyPolycy| Title | errore|
| Title |            | Surname | emailAddress | Phonenumber | alreadyClient | youngerThan26 | earingAids |privacyPolycy| Title | errore|
| Title | First name |         | emailAddress | Phonenumber | alreadyClient | youngerThan26 | earingAids |privacyPolycy| Title | errore|
| Title | First name | Surname |              | Phonenumber | alreadyClient | youngerThan26 | earingAids |privacyPolycy| Title | errore|
| Title | First name | Surname | emailAddress |             | alreadyClient | youngerThan26 | earingAids |privacyPolycy| Title | errore|
| Title | First name | Surname | emailAddress | Phonenumber |               | youngerThan26 | earingAids |privacyPolycy| Title | errore|
| Title | First name | Surname | emailAddress | Phonenumber | alreadyClient |               | earingAids |privacyPolycy| Title | errore|
| Title | First name | Surname | emailAddress | Phonenumber | alreadyClient | youngerThan26 |            |privacyPolycy| Title | errore|
| Title | First name | Surname | emailAddress | Phonenumber | alreadyClient | youngerThan26 | earingAids |             | Title | errore|
| Title | First name | Surname | emailAddress | Phonenumber | alreadyClient | youngerThan26 | earingAids |privacyPolycy| Title | errore|



