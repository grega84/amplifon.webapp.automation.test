package com.amplifon.appointment;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import test.automation.core.XCucumber;
import test.automation.core.annotation.Ui;


@RunWith(XCucumber.class)
@CucumberOptions(plugin = { "json:target/reports/BookAnAppointment.json" },features = {"src/test/resources/com/amplifon/appointment/BookAnAppointment.feature"})
@Ui
public class BookAnAppointmentTest {

}
